import React from "react";
import { faPlus, faMinus, faTrash } from "@fortawesome/free-solid-svg-icons";
import Buttons from "../shared-components/buttons";

export default function Products({
  filteredList,
  items,
  updateTotalItemCount,
  setItems,
}) {
  /**
   * This function call when click on plus/increase icon
   * @param id   this parameter get id of click item
   * Increase this clicked item quantity value and update the total amount value
   */
  const handleQuantityIncrease = (id) => {
    // filteredList[id].quantity++;
    const newItems = [...items];

    newItems.forEach((element) => {
      if (element.id === id) element.quantity++;
    });
    setItems(newItems);
    calculateTotal();
  };

  /**
   * This function call when click on minus/decrease icon
   * @param id   this parameter get id of click item
   * decrease this clicked item quantity value and update the total amount value
   */
  const handleQuantityDecrease = (id) => {
    const newItems = [...items];

    newItems.forEach((element) => {
      if (element.id === id) element.quantity--;
    });
    setItems(newItems);
    calculateTotal();
  };

  /**
   * This function call when click on remove icon
   * @param id   this parameter get id of click item
   * change this clicked item quantity value to 0 and also update the product list and total amount value
   */
  const handleRemoveItem = (id) => {
    const newItems = [...items];

    newItems.forEach((element) => {
      if (element.id === id) element.quantity = 0;
    });
    setItems(newItems);
    calculateTotal();
  };

  /**
   * This function called when update the total amount variable
   * @param no prams required for this method
   * using the reduce get every item whose deleted value is false and multiple the price with item quantity and added this value to total amount;
   */
  const calculateTotal = () => {
    const totalItemCount = items.reduce((total, item) => {
      return +(total + item.quantity * item.price).toFixed(2);
    }, 0);

    updateTotalItemCount(totalItemCount);
  };

  return (
    <div className="item-list">
      {filteredList.map((item) => (
        <div className="item-container" key={item.id}>
          <div className="item-name">
            <img className="item-image" src={item.img} alt=" "></img>
            <span className="item-detail">
              <h5>{item.name}</h5>
              <p>$ {item.price}</p>
            </span>
          </div>
          <span className="text-center">
            <small>Qty</small>
            <div className="quantity">
              <Buttons
                className="text-danger"
                icon={faMinus}
                onClick={() => handleQuantityDecrease(item.id)}
                disabled={item.quantity === 0 ? true : false}
              />
              <span> {item.quantity} </span>
              <Buttons
                className="text-success"
                icon={faPlus}
                onClick={() => handleQuantityIncrease(item.id)}
              />
            </div>
            <Buttons
              className="text-danger"
              icon={faTrash}
              onClick={() => handleRemoveItem(item.id)}
            />
          </span>
        </div>
      ))}
    </div>
  );
}
