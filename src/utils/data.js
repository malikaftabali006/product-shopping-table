export const COLORS = [
  { id: 0, value: "White", label: "White" },
  { id: 1, value: "Black", label: "Black" },
  { id: 2, value: "Blue", label: "Blue" },
  { id: 3, value: "Stone", label: "Stone" },
  { id: 4, value: "Red", label: "Red" },
];
