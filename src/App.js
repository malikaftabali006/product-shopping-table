import React, { useEffect, useMemo, useState } from "react";
import "./index.css";
import { COLORS } from "./utils/data";
import { getList } from "./services/productServices";
import Products from "./containers/products";

const App = () => {
  const [items, setItems] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState();
  const [totalItemCount, setTotalItemCount] = useState(0);

  // Call product api and set list on page load
  useEffect(() => {
    async function fetchListAPI() {
      try {
        let response = await getList();
        if (response.status === 200) {
          let data = await response.json();
          data.forEach((element) => {
            element["quantity"] = 0;
          });
          setItems(data);
        } else {
          throw new Error("Error fetching products list");
        }
      } catch (error) {
        throw error;
      }
    }

    fetchListAPI();
  }, []);

  /**
   * This function called when change in color filter select field
   * @param event prams required for this method
   * set the selected option value to filter variable
   */
  const handleFilter = (event) => {
    setSelectedCategory(event.target.value);
  };

  // Function to get filtered list
  const getFilteredList = () => {
    // Avoid filter when selectedCategory is null
    if (!selectedCategory) {
      return items;
    }
    return items.filter((item) => item.colour === selectedCategory);
  };

  // Avoid duplicate function calls with useMemo
  var filteredList = useMemo(getFilteredList, [selectedCategory, items]);

  function updateTotalItemCount(value) {
    setTotalItemCount(value);
  }

  return (
    <>
      <h2 className="main-title">Product Listing Task</h2>

      <div className="app-background">
        <div className="main-container">
          <div className="d-flex text-center">
            <label htmlFor="filter">Color Filter:</label>
            <select
              name="Colors"
              id="filter"
              value={selectedCategory}
              onChange={(event) => handleFilter(event)}
            >
              <option value="">All</option>
              {COLORS.map((e) => {
                return (
                  <option key={e.id} value={e.value}>
                    {e.label}
                  </option>
                );
              })}
            </select>
          </div>

          <Products
            filteredList={filteredList}
            items={items}
            setItems={setItems}
            updateTotalItemCount={updateTotalItemCount}
          />

          {filteredList.length === 0 ? (
            <h5 className="text-center my-5">No product found</h5>
          ) : null}
          <div className="total">Total: ${totalItemCount}</div>
        </div>
      </div>
    </>
  );
};

export default App;
