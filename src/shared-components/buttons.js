import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Buttons = (props) => {

    return (
        <button className={props.className} disabled={props.disabled} onClick={props.onClick}>
                <FontAwesomeIcon icon={props.icon} />
        </button>
    )

}

export default Buttons;
